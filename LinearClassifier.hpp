#ifndef LinearClassifier_HPP
#define LinearClassifier_HPP

#include <string>
#include <vector>
#include <fcntl.h>
#include <unistd.h>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>

#include "config.hpp"
#include "commonTools.hpp"

class LinearClassifier
{
private:
    double calculateScore(double, double, double, double, double);
    std::string pathToWeights;
    std::string pathToDataSet;
    std::vector<std::vector <double>> weights;
    std::vector<std::vector <double>> dataSet;
    std::string results;
    std::string lcNumber;
public:
    LinearClassifier(std::string, std::string, std::string);
    void readWeights();
    std::string findMatchClass(double, double);
    void sendDataToVoter();
    void readDataSet();
    void classify();
};


#endif