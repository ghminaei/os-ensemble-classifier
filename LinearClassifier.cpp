#include "LinearClassifier.hpp"


LinearClassifier::LinearClassifier(std::string pathToWeightsInp, std::string pathToDataSetInp, std::string lcNumberInp)
    : pathToWeights(pathToWeightsInp), pathToDataSet(pathToDataSetInp), lcNumber(lcNumberInp)
{ }

void LinearClassifier::classify()
{
    std::string newResult;
    readDataSet();
    readWeights();
    for (int i = 0; i < dataSet.size(); i++) {
        newResult = findMatchClass(dataSet[i][0], dataSet[i][1]);
        results += newResult + SEPCHAR;
    }
    sendDataToVoter();
}

void LinearClassifier::readWeights()
{
    readFromCSV(pathToWeights, weights);
}

void LinearClassifier::readDataSet()
{
    readFromCSV(pathToDataSet, dataSet);
}

std::string LinearClassifier::findMatchClass(double length, double width)
{
    double maxScore = calculateScore(weights[0][0], weights[0][1], weights[0][2], length, width);
    int classIndex = 0;
    for (int i = 1; i < weights.size(); i++) {
        double newScore = calculateScore(weights[i][0], weights[i][1], weights[i][2], length, width);
        if (newScore > maxScore) {
            maxScore = newScore;
            classIndex = i;
        }
    }
    return std::to_string(classIndex);
}

void LinearClassifier::sendDataToVoter()
{
    std::string pipePaths = "";
    pipePaths = LINEARCLASSIFIERSPIPE + (std::string)"_" + lcNumber;
    int fd = open(pipePaths.c_str(), O_WRONLY);
    write(fd, results.c_str(), (results.length())+1);
    close(fd);
}

double LinearClassifier::calculateScore(double beta0, double beta1, double bias, double length, double width)
{
    return length*beta0 + width*beta1 + bias;
}

