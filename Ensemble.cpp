#include "Ensemble.hpp"

Ensemble::Ensemble(std::string pathToWeightsDirInp, std::string pathToDataSetDirInp)
    // : pathToWeightsDir(pathToWeightsDirInp), pathToDataSetDir(pathToDataSetDirInp)
{ 
    pathToWeightsDir = "./" + pathToWeightsDirInp + "/";
    pathToDataSetDir = "./" + pathToDataSetDirInp + "/";
    initialCalculations();
}

void Ensemble::initialCalculations()
{
    std::vector<std::string> weightFiles;
    DIR *dir = opendir(pathToWeightsDir.c_str());
    if (dir == NULL) {
        std::cout << "Directory Not Found!" << std::endl;
    }
    struct dirent *entry;
    while (entry = readdir(dir)) {
        if (entry->d_type == DT_DIR) {
            continue;
        }
        if (entry->d_type == DT_REG) {
            std::string tempDir;
            tempDir = pathToWeightsDir + "/" + entry->d_name;
            weightFiles.push_back(tempDir);
        }
    }
    std::vector<std::vector <double>> weights;
    linearClassifiersCount = weightFiles.size();
    readFromCSV(makePathToWeight(0) , weights);
    classCount = weights.size();
}

void Ensemble::makeChildProcessesAndPipes()
{
    std::string pipePaths = "";
    pipePaths = LINEARCLASSIFIERSPIPE + (std::string)"_";
    mkfifo(pipePaths.c_str(), 0666);
    for (int i = 0; i < linearClassifiersCount; i++)
    {
        unlink((pipePaths + std::to_string(i)).c_str());
        mkfifo((pipePaths + std::to_string(i)).c_str(), 0666);
    }

    unlink(VOTERPIPE);
    mkfifo(VOTERPIPE, 0666); 

    int pipesFd[linearClassifiersCount][2];
    for (int i = 0; i < linearClassifiersCount; i++){
        if (pipe(pipesFd[i])== -1){
            perror("pipe failed!");
            exit(1);
        }
        int pid = fork();
        if (pid == 0){
            close(pipesFd[i][1]);
            char msg[1000]; 
            read(pipesFd[i][0], msg, 1000); 
            close(pipesFd[i][0]);
            char* argv[3] = {LINEARCLASSIFIER, msg, NULL};
            execv(LINEARCLASSIFIER, argv);
        }
        else if(pid > 0){
            close(pipesFd[i][0]);
            std::string paths;
            paths = makePathToWeight(i) + SEPCHAR + pathToDataSetDir + DATASETFILE + SEPCHAR + std::to_string(i);
            write(pipesFd[i][1], paths.c_str(), (paths.length())+1);
            close(pipesFd[i][1]);
        }
        else{
            perror("fork failed!");
            exit(1);
        }
    }

    int voterPipe[2];
    if (pipe(voterPipe)== -1){
        perror("pipe failed!");
        exit(1);
    }
    int pid = fork();
    if (pid == 0){
        close(voterPipe[1]);
        char msg[1000]; 
        read(voterPipe[0], msg, 1000); 
        close(voterPipe[0]);
        char* argv[3] = {VOTER, msg, NULL};
        execv(VOTER, argv);
    }
    else if(pid > 0){
        close(voterPipe[0]);
        std::string msgToVoter = std::to_string(linearClassifiersCount) + SEPCHAR + std::to_string(classCount);
        write(voterPipe[1], msgToVoter.c_str(), (msgToVoter.length())+1);
        close(voterPipe[1]);
    }
    else{
        perror("fork failed!");
        exit(1);
    }
    std::ifstream fd;
    fd.open(VOTERPIPE, std::fstream::in);
    std::getline(fd, voterResult);
}

std::string Ensemble::makePathToWeight(int classifierNum)
{
    return pathToWeightsDir + CLASSIFIERFILE + std::to_string(classifierNum) + DOTCSV;
}


void Ensemble::waitForClassifiersAndVoter()
{
    for (int i = 0; i < linearClassifiersCount+1; i++) {
        wait(NULL);
    }
}

void localReadFromCSV(std::string path, std::vector<std::vector <std::string>>& labels)
{
	std::fstream fin; 
	fin.open(path, std::ios::in);
	std::vector<std::string> row; 
	std::string word, temp, gar; 
    getline(fin, gar);
	while (fin >> temp) {
		row.clear();
		std::stringstream s(temp); 
		while (getline(s, word, ','))
			row.push_back(word);
        labels.push_back(row);
	}
}

void Ensemble::calculateAccuracy()
{
    std::vector<std::string> parsedStr;
    std::vector<std::vector<std::string>> labels;
    parseOnChar(voterResult, parsedStr, SEPCHAR);
    localReadFromCSV(pathToDataSetDir + LABELSFILE, labels);
    double similarCount = 0;
    for (int i = 0; i < labels.size(); i++)
    {
        if (parsedStr[i] == labels[i][0]){
            similarCount++;
        }
    }
    printf("Accuracy: %.2f%%\n", similarCount / labels.size() * 100);
}