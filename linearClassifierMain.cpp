#include "LinearClassifier.hpp"
#include "config.hpp"

int main(int argc, char *argv[])
{
    std::vector<std::string> parsedString;
    parseOnChar(argv[1], parsedString, SEPCHAR);
    LinearClassifier* lc;
    lc = new LinearClassifier(parsedString[0], parsedString[1], parsedString[2]);
    lc->classify();
}