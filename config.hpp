#ifndef Config_HPP
#define Config_HPP

#define DATASETFILE "dataset.csv"
#define LABELSFILE "labels.csv"
#define CLASSIFIERFILE  "classifier_"
#define DOTCSV ".csv"

#define LINEARCLASSIFIER "./linearClassifier"
#define VOTER "./voter"

#define VOTERPIPE "./pipe/voterPipe"
#define LINEARCLASSIFIERSPIPE "./pipe/lcPipe"

#define SEPCHAR '#'
#define SEPSTR "#"

#endif