CC=g++
CF=-std=c++11 

all:  
	make Ensemble
	make linearClassifier
	make Voter
	make clean

Ensemble: ensembleMain.o Ensemble.o commonTools.o
	$(CC) $(CF) ensembleMain.o Ensemble.o commonTools.o -o EnsembleClassifier.out

ensembleMain.o: ensembleMain.cpp
	$(CC) $(CF) -c ensembleMain.cpp

Ensemble.o: Ensemble.cpp
	$(CC) $(CF) -c Ensemble.cpp

commonTools.o: commonTools.cpp
	$(CC) $(CF) -c commonTools.cpp

linearClassifier: linearClassifierMain.o LinearClassifier.o commonTools.o
	$(CC) $(CF) linearClassifierMain.o LinearClassifier.o commonTools.o -o linearClassifier

linearClassifierMain.o: linearClassifierMain.cpp
	$(CC) $(CF) -c linearClassifierMain.cpp

LinearClassifier.o: LinearClassifier.cpp
	$(CC) $(CF) -c LinearClassifier.cpp

Voter: voterMain.o commonTools.o
	$(CC) $(CF) voterMain.o commonTools.o -o voter

voterMain.o: voterMain.cpp
	$(CC) $(CF) -c voterMain.cpp
	
clean:
	rm *.o
