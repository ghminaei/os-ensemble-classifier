#include "commonTools.hpp"

void readFromCSV(std::string path, std::vector<std::vector <double>>& weights)
{
	std::fstream fin; 
	fin.open(path, std::ios::in);
	std::vector<double> row; 
	std::string word, temp, gar; 
	getline(fin, gar);
	while (fin >> temp) {
		row.clear();
		std::stringstream s(temp); 
		while (getline(s, word, ','))
			row.push_back(std::stod(word));
        weights.push_back(row);
	}
	fin.close();
}

void parseOnChar(std::string str, std::vector<std::string>& parsedString, char ch)
{ 
	std::string word;
	std::stringstream s(str); 
		while (getline(s, word, ch))
			parsedString.push_back(word);
}