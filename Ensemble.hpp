#ifndef Ensemble_HPP
#define Ensemble_HPP

#include <string>
#include <vector>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>

#include "config.hpp"
#include "commonTools.hpp"

class Ensemble
{
private:
    std::string pathToWeightsDir;
    std::string pathToDataSetDir;
    std::string makePathToWeight(int);
    void initialCalculations();
    int linearClassifiersCount;
    int classCount;
    std::string voterResult;
public:
    Ensemble(std::string, std::string);
    void makeChildProcessesAndPipes();
    void waitForClassifiersAndVoter();
    void calculateAccuracy();
};


#endif