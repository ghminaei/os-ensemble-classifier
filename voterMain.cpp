#include "config.hpp"

#include <string>
#include <vector>
#include <fcntl.h>
#include <unistd.h>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <bits/stdc++.h> 
#include <iostream>
#include <sys/stat.h>

#include "commonTools.hpp"

using namespace std;

int maxClassNum(vector<double> list)
{
	float max = list[0];
	int bestClassIndex = 0;

	for (int i = 1; i < list.size(); i++)
		if (list[i] > max) {
			max = list[i];
			bestClassIndex = i;
		}

	return bestClassIndex;
}

int bestMatchClass(vector<vector<int>> classifiedResults, int j, int classNum, int lcNumber)
{
	vector<double> classCountPair(classNum, 0);

	for (int i = 0; i < lcNumber; i++)
		classCountPair[classifiedResults[i][j]] += 1;

	int final_class = maxClassNum(classCountPair);
	return final_class;
}

vector<int> voter(vector<vector<int>> classifiedResults, int classNum, int lcNumber)
{
	vector<int> votedResult;
	for (int j = 0; j < classifiedResults[0].size(); j++)
		votedResult.push_back(bestMatchClass(classifiedResults, j, classNum, lcNumber));
	return votedResult;
}

void parseOnCharInt(std::string str, std::vector<int>& parsedString, char ch)
{ 
	std::string word;
	std::stringstream s(str); 
	while (getline(s, word, ch))
		parsedString.push_back(stoi(word));
}

std::string prepareData(std::vector<std::string>& classifiedResults, int lcNumber, int classNumber)
{
    std::vector<std::vector<int>> parsedResults;
    std::vector<int> parsedStr;
    std::vector<std::pair <int, int>> classCountVec;
    bool pairFound = false;
    int maxCount;
    int candidate;
    std::string finalVote = "";
    for (int i = 0; i < classifiedResults.size(); i++) {
        parsedStr.clear();
        parseOnCharInt(classifiedResults[i], parsedStr, SEPCHAR);
        parsedResults.push_back(parsedStr);
    }
    vector<int> res = voter(parsedResults, classNumber, lcNumber);
    for (int i = 0; i < res.size(); i++) {
        finalVote += to_string(res[i]) + SEPSTR;
    }
    return finalVote;
}

int main(int argc, char *argv[])
{
    std::string votedResult;
    std::string pipePaths = "";
    std::string linearClassifierData;
    std::vector<std::string> parsedStringInp;
    parseOnChar(argv[1], parsedStringInp, SEPCHAR);
    int lcNumber = stoi(parsedStringInp[0]);
    int classNumber = stoi(parsedStringInp[1]);
    std::vector<std::string> classifiedResults(lcNumber);

    for (int i = 0; i < lcNumber; i++) {
        linearClassifierData = "";
        ifstream fd;
        pipePaths = LINEARCLASSIFIERSPIPE + (std::string)"_" + std::to_string(i);
        fd.open(pipePaths, std::fstream::in);
        getline(fd, linearClassifierData, '\0');
        classifiedResults[i] = linearClassifierData;
        fd.close();
    }
    votedResult = prepareData(classifiedResults, lcNumber, classNumber);
    
    int fd;
	fd = open(VOTERPIPE, O_WRONLY);
	write(fd, votedResult.c_str(), (votedResult.length())+1);
	close(fd);
    return 0;
}