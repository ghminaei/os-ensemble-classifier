#include "Ensemble.hpp"
#include "config.hpp"

int main(int argc, char *argv[])
{
    Ensemble* e = new Ensemble(argv[2], argv[1]);
    e->makeChildProcessesAndPipes();
    e->calculateAccuracy();
    e->waitForClassifiersAndVoter();
}